<?php
namespace app\helpers;

class TotalHelper
{
    public static function getTotal($array, $fieldName)
    {
        $total = 0;
        foreach ($array as $value){
            $total += $value[$fieldName];
        }
        return $total;
    }
}