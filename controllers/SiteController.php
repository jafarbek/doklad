<?php

namespace app\controllers;

use app\models\Category;
use app\models\Product;
use app\services\ElasticClient;
use Yii;
use yii\caching\DbDependency;
use yii\caching\TagDependency;
use yii\db\Expression;
use yii\filters\HttpCache;
use yii\filters\PageCache;
use yii\web\Controller;
use yii\web\Response;
use app\models\ContactForm;

class SiteController extends Controller
{
    private \Elasticsearch\Client $client;

    public function __construct($id, $module, $config = [])
    {
        $this->client = (new ElasticClient())->client;
        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        $query = Yii::$app->request->get('query');
        $start = microtime(true);
        $docs = [];
        if ($query) {
            $params = [
                'index' => 'products',
                'size' => 1000,
                'body' => [
                    'query' => [
                        'match' => [
                            'description' => $query
                        ]
                    ]
                ]
            ];
            $response = $this->client->search($params);

            $docs = ($response['hits']['hits']); // documents
        }
        $end = microtime(true);
        $spend = round(($end - $start)*1000).'ms';

        return $this->render('elastic', [
            'docs' => $docs,
            'query' => $query,
            'spend' => $spend
        ]);
    }

    public function actionPostgres()
    {
        $query = Yii::$app->request->get('query');
        $start = microtime(true);
        $docs = [];
        if ($query) {
            $docs = Product::find()
                ->select([
                    'name',
                    'description',
                    new Expression("ts_headline('russian', description, plainto_tsquery('russian','$query'),'StartSel=<em>, StopSel=</em>, MaxFragments=0, MinWords=5, MaxWords=9') as query"),
                    'created_at',
                    new Expression("document <=> plainto_tsquery('russian','$query') as distance")
                ])
                ->where(new Expression("document @@ plainto_tsquery('russian','$query')"))
                // RUM index ishlatilgan. Buni desc berib bolmaydi
                ->orderBy(new Expression("document <=> plainto_tsquery('russian','$query')"))
                ->limit(1000)
                ->asArray()
                ->all();
        }
//debug($docs);
        $end = microtime(true);
        $spend = round(($end - $start)*1000).'ms';

        return $this->render('postgres', [
            'docs' => $docs,
            'query' => $query,
            'spend' => $spend
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
