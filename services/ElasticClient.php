<?php

namespace app\services;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

class ElasticClient
{
    public array $hosts = ['http://127.0.0.1:9200'];
    public Client $client;
    public function __construct()
    {
        $this->client = ClientBuilder::create()
            ->setSSLVerification(false)
            ->setHosts($this->hosts)
            ->build();
    }
}