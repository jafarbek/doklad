<?php

namespace app\services;

use app\models\Product;
use Elasticsearch\Client;

class ProductIndexer
{
    public Client $client;
    public function __construct()
    {
        $this->client = (new ElasticClient())->client;
    }

    public function clear(): void
    {
        $this->client->deleteByQuery([
            'index' => 'products',
            'body' => [
                'query' => [
                    'match_all' => new \stdClass(),
                ],
            ],
        ]);
    }

    public function index(Product $product): void
    {
        $this->client->index([
            'index' => 'products',
            'id' => $product->id,
            'body' => [
                'id' => $product->id,
                'name' => $product->name,
                'category_id' => $product->category_id,
                'description' => $product->description,
                'created_at' => $product->created_at
                    ? \DateTime::createFromFormat('Y-m-d H:i:s',$product->created_at)->format(DATE_ATOM)
                    : null,
            ],
        ]);
    }

    public function remove(Product $product): void
    {
        $this->client->delete([
            'index' => 'products',
            'id' => $product->id,
        ]);
    }
}