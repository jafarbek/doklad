<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Product;
use app\services\ProductIndexer;
use Elasticsearch\Common\Exceptions\Missing404Exception;
use yii\console\Controller;

class SearchController extends Controller
{
    private ProductIndexer $indexer;

    public function __construct($id, $module, $config = [])
    {
        $this->indexer = new ProductIndexer();
        parent::__construct($id, $module, $config);
    }

    public function actionInitProducts(): bool|string
    {

        $client = (new ProductIndexer())->client;
        try {
            $client->indices()->delete([
                'index' => 'products'
            ]);
        } catch (Missing404Exception $e) {
            debug($e);
        }
        $params = [
            'index' => 'products',
            'body' => [
                'mappings' => [
                    '_source' => [
                        'enabled' => true,
                    ],
                    'properties' => [
                        'id' => [
                            'type' => 'integer',
                        ],
                        'name' => [
                            'type' => 'text',
                        ],
                        'category_id' => [
                            'type' => 'integer',
                        ],
                        'description' => [
                            'type' => 'text',
                        ],
                        'created_at' => [
                            'type' => 'date',
                        ],
                    ],
                ],
                'settings' => [
                    'analysis' => [
                        'char_filter' => [
                            'replace' => [
                                'type' => 'mapping',
                                'mappings' => [
                                    '&=> and '
                                ],
                            ],
                        ],
                        'filter' => [
                            'word_delimiter' => [
                                'type' => 'word_delimiter',
                                'split_on_numerics' => false,
                                'split_on_case_change' => true,
                                'generate_word_parts' => true,
                                'generate_number_parts' => true,
                                'catenate_all' => true,
                                'preserve_original' => true,
                                'catenate_numbers' => true,
                            ],
                            'trigrams' => [
                                'type' => 'ngram',
                                'min_gram' => 4,
                                'max_gram' => 6,
                            ],
                        ],
                        'analyzer' => [
                            'default' => [
                                'type' => 'custom',
                                'char_filter' => [
                                    'html_strip',
                                    'replace',
                                ],
                                'tokenizer' => 'whitespace',
                                'filter' => [
                                    'lowercase',
                                    'word_delimiter',
                                    'trigrams',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
        $response = $client->indices()->create($params);

        return print_r($response);
    }

    public function actionReindex()
    {
        $bool = true;
        $k = 0;
        $batch = 100;
        while ($bool) {
            $products = Product::find()
                ->where(['>','id',3183300])
                ->orderBy('id')->offset($k)->limit($batch)->all();

            if ($products){
                $k += $batch;
                foreach ($products as $product) {
                    $this->indexer->index($product);
                }

                if ($k % $batch === 0) {
                    print 'Successfully created ' . $k . PHP_EOL;
                }
            }else{
                $bool = false;
            }

        }

    }

}
