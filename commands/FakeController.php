<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Faker\Factory;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class FakeController extends Controller
{
    public function actionIndex()
    {
        $this->generateCategories();
        $this->generateProducts();
        return ExitCode::OK;
    }

    private function generateCategories(){
        Yii::$app->db->createCommand()->batchInsert('categories',
            ['name','created_at','updated_at'],
            [
                ['Notebooklar',date('Y-m-d H:i:s'),date('Y-m-d H:i:s')],
                ['Kompyuterlar',date('Y-m-d H:i:s'),date('Y-m-d H:i:s')],
                ['Telefonlar',date('Y-m-d H:i:s'),date('Y-m-d H:i:s')],
                ['Televizorlar',date('Y-m-d H:i:s'),date('Y-m-d H:i:s')],
                ['Mebellar',date('Y-m-d H:i:s'),date('Y-m-d H:i:s')],
                ['Uy uchun texnika',date('Y-m-d H:i:s'),date('Y-m-d H:i:s')],
                ['Boshqa elektronika',date('Y-m-d H:i:s'),date('Y-m-d H:i:s')]
            ]
        )->execute();
        print 'Categories generation is complete!'.PHP_EOL;
    }

    private function generateProducts()
    {
        $faker = Factory::create('ru_RU');
        $k= 0;
        for($i = 0; $i < 3250; $i++){
            $products = [];
            for ($j = 0; $j < 1000; $j++){
                $products[] = [
                    $faker->name,
                    $faker->realText(rand(200,2000)),
                    rand(1,7),
                    date('Y-m-d H:i:s'),
                ];
            }
            Yii::$app->db->createCommand()->batchInsert('products',
                ['name','description','category_id','created_at'],
                $products
            )->execute();
            $k += 1000;
            print 'Inserted: '.$k.' rows'.PHP_EOL;
            unset($products);
        }

        print 'Products generation is complete!'.PHP_EOL;
    }

}
