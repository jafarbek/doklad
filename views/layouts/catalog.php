<?php

/* @var $this \yii\web\View */
/* @var $content string */
?>

<?php $this->beginContent('@app/views/layouts/layout.php')?>
<div class="row">
<!-- ФРАГМЕНТНИ КЕШЛАШ -->
    <div class="col-lg-2 col-md-2 col-sm-12">
        <?php if ($this->beginCache("side-bar-widget",
            [
                'dependency'=> [
                    'class' => \yii\caching\TagDependency::class,
                    'tags' => [\app\repositories\IncomeGoodRepository::allTagDependency(), \app\repositories\SaleGoodRepository::allTagDependency()]
                ],
                'duration'=>3600,
                'variations'=> $action.$good_id
        ]
        )) :?>
        <?php echo \app\widgets\SideBarWidget::widget();
            ?>
        <?php $this->endCache(); endif; ?>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-12">
        <?= $content ?>
    </div>
</div>
<?php $this->endContent()?>