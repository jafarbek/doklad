<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<style>
    em{
        background: yellow;
    }
</style>
<div class="site-index">
    <form action="" method="GET" class="">
        <div>
            <div class="col-md-9">
                <?= \yii\helpers\Html::input('text', 'query', $query, ['class' => 'form-text form-control']) ?>
            </div>
            <div class="col-md-3">
                <button class="btn btn-success"><?= Yii::t('yii', 'search') ?></button>

            </div>
        </div>
    </form>
    <br><br><br>
    <?php if (!empty($docs)):?>
    <h5 style="color: red">Spend time <?=$spend?></h5>
    <table class="table table-bordered">
        <thead>
        <th>#</th>
        <th>Name</th>
        <th>Slice description</th>
        <th>Description</th>
        <th>distance</th>
        </thead>
        <tbody>
        <?php $i = 0; foreach ($docs as $doc): ?>
            <tr class="wrap-tr">
                <td><?=++$i?></td>
                <td><?=$doc['name']?></td>
                <td><?=$doc['query']?></td>
                <td><?=$doc['description']?></td>
                <td><?=$doc['distance']?></td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
    <?php endif;?>
</div>
