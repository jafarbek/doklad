<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sale}}`.
 */
class m210215_170057_create_products_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'category_id' => $this->integer(),
            'created_at' => $this->timestamp(),
        ]);

        $this->createIndex('products_category_id_idx','products','category_id');
        $this->addForeignKey(
            'products_category_id_FK',
            'products',
            'category_id',
            'categories',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('products_category_id_FK','products');
        $this->dropIndex('products_category_id_idx','products');
        $this->dropTable('{{%products}}');
    }
}
