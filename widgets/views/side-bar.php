<?php
/* @var $this \yii\web\View */
/* @var $categories \app\models\Category[] */
?>

<div class="panel panel-footer">
    <div class="panel panel-heading"><?= t("Kategoriyalar") ?></div>
    <ul class="nav nav-pills nav-stacked" role="tablist">
        <?php foreach ($categories as $category): ?>
            <li role="presentation">
                <a href="#"><?= $category['name'] ?>
                    <span class="badge"><?= $category['p_count'] ?></span>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
