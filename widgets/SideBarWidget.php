<?php


namespace app\widgets;


use app\models\Category;
use app\models\Product;
use phpDocumentor\Reflection\DocBlock\Tag;
use yii\bootstrap\Widget;
use yii\caching\TagDependency;
use yii\db\ActiveQuery;

class SideBarWidget extends Widget
{

    public function run()
    {
        $categories = Category::find()
                ->select(['name','p_count'])
                ->leftJoin(
                    "(select category_id, count(*) as p_count 
                            from products group by category_id) as p",
                    'categories.id = p.category_id'
                )
                ->asArray()
                ->all();

        return $this->render('side-bar', [
            'categories' => $categories
        ]);
    }













//    public function run()
//    {
//
//        TagDependency::invalidate(\Yii::$app->redis,['products']);
//        /** @var $redis \yii\redis\Cache */
//        $redis = \Yii::$app->redis;
//        $cache = \Yii::$app->cache;
////        $redis->flush();
//        $categories = $redis->getOrSet('salom', function () {
//            return Category::find()
//                ->select(['name','p_count'])
//                ->leftJoin(
//                    "(select category_id, count(*) as p_count from products group by category_id) as p",
//                    'categories.id = p.category_id'
//                )
//                ->asArray()
//                ->all();
//        },0,new TagDependency(['tags'=>['categories','products']]));
//        return $this->render('side-bar', [
//            'categories' => $categories
//        ]);
//    }
}